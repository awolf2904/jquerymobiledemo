Posts = new Meteor.Collection('posts');

//ownsDocument see lib/permissions.js

Posts.allow({
	update: ownsDocument,
	remove: ownsDocument
});

Posts.deny({
  update: function(doc, fields, modifier) {
  	//console.log("update in progress");
    // may only edit the following fields:
    // console.log(modifier);
    console.log((_.without(modifier, 'url', 'title', 'message').length > 0));
    return (_.without(modifier, 'url', 'title', 'message').length > 0);
  }
});

// deny callback will always be called before a successful update so lastmodified can be stored --> it's a hack
// but beforeUpdate callback is not available yet
/*
Posts.deny({
  update: function(userId, doc) {
    doc.lastModified = +(new Date());
    return false;
  }
});*/

/*
Posts.allow({
	insert: function(userId, doc) {
		//only allow posting if you are logged in
		return !! userId; // short-hand for Boolean(userId)
	}
});

Posts.allow({
	remove: function(userId, doc) {
		//only allow posting if you are logged in
		return !! userId; // short-hand for Boolean(userId)
	}
});*/

Meteor.methods({
	post: function(postAttributes) {
		var user = Meteor.user(),
			postWithSameLink = Posts.findOne({url: postAttributes.url});

		//console.log(postWithSameLink);
		// ensure the user is logged in
		if ( !user )
			throw new Meteor.Error(401, "You need to login to post new stories!");

		// ensure the post has a title
		if ( !postAttributes.title )
			throw new Meteor.Error(422, 'Please fill in a headline');

		// check that there ar no previous posts with the same link

		if ( postAttributes.url && postWithSameLink ) {
			throw new Meteor.Error(302, 'This link has already been posted',
				postWithSameLink);
		}

		// pick out the whitelisted keys
		//console.log(user.username);
		
		var post = _.extend(_.pick(postAttributes, 'url', 'title', 'message'), {
			userId: user._id,
			author: user.username,
			submitted: new Date().getTime(),
			commentsCount: 0,
			upvoters: [],
			votes: 0
		});
    
    var postId = Posts.insert(post);
    return postId;
	},

	upvote: function(postId) {
		var user = Meteor.user();
		//ensure the user is logged in

		if ( !user ) throw new Meteor.Error(401, "You need to login to upvote");

		var post = Posts.findOne(postId);

		if (!post)
			throw new Meteor.Error(422, 'Post not found');

		if (_.include(post.upvoters, user._id))
      throw new Meteor.Error(422, 'Already upvoted this post');

		Posts.update({
			_id: post._id,
			upvoters: {$ne: user._id} //not exits in upvoters
			},{
      $addToSet: {upvoters: user._id},
      $inc: {votes: 1}
    });

	}/*,
	getUserList: function() {
		//console.log(Users.find().fetch());
		//var userList = Users.find().fetch();
		var posts = Posts.find();
		var names = _.pluck(posts, 'username');
		console.log(names);
		return names;
	}*/
});