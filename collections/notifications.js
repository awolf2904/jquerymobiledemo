Notifications = new Meteor.Collection('notifications');

Notifications.allow({
	//update: ownsDocument,
	remove: ownsDocument
});

createCommentNotification = function(comment){

	var post = Posts.findOne(comment.postId);
	//console.log(comment.userId);
	//console.log(post);
	if ( comment.userId !== post.userId) {
		Notifications.insert({
			userId: post.userId,
			postId: post._id,
			commentId: comment._id,
			commenterName: comment.author,
			read: false
		});
		//console.log(Notifications.find().fetch());
	}
};