/*Meteor.publish('posts', function(limit){
	//return Posts.find(); //all posts
	return Posts.find({});
});*/

/*Meteor.publish('posts', function(limit) {
  return Posts.find({}, {limit: limit});
});*/

Meteor.publish('newPosts', function(limit) {  
	return Posts.find({}, {sort: {submitted: -1}, limit: limit});
});

Meteor.publish('bestPosts', function(limit) {  
	return Posts.find({}, {sort: {votes: -1, submitted: -1}, limit: limit});
});

Meteor.publish('comments', function(postId) {
	return Comments.find({postId: postId});
});

Meteor.publish('notifications', function() {
	return Notifications.find({userId: this.userId});
});