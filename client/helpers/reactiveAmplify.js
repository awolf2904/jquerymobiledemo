/*
* jQueryMobiledemo - Show case for jQuery Mobile and Meteor
* http://jquerymobiledemo.meteor.com
*
* Copyright 2013 A. Wolf / AWolf2904@gmail.com
* Released under the GNU AGPL license.
* <http://www.gnu.org/licenses/>
*
*/

ReactiveAmplifyStore = {
  //keys: {},
  deps: {},

  get: function(key) {
    this.ensureDeps(key);
    this.deps[key].depend();
    // console.log('get for key: '+ key); // here you can check when the store gets called --> reacitivty testing
    return amplify.store(key);
  },

  set: function(key, value) {
    amplify.store(key, value);
    this.deps[key].changed();
  },

  ensureDeps: function(key) {
    if ( ! this.deps[key] )
      this.deps[key] = new Deps.Dependency();
  }
};