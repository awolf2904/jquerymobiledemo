/*
* jQueryMobiledemo - Show case for jQuery Mobile and Meteor
* http://jquerymobiledemo.meteor.com
*
* Copyright 2013 A. Wolf / AWolf2904@gmail.com
* Released under the GNU AGPL license.
* <http://www.gnu.org/licenses/>
*
*/

Subscriptions = {
  bestPosts:     Meteor.subscribeWithPagination('bestPosts', 10),
  newPosts:      Meteor.subscribeWithPagination('newPosts', 10),
  notifications: Meteor.subscribe('notifications')
};

Deps.autorun(function() {
  Subscriptions.comments = Meteor.subscribe('comments', Session.get('currentPostId'));
});


Router.map(function () {

  // ------------------------- jQm Demo routes ---------------------------
  this.route('home', {
		path: '/',
    template: 'home',
		controller: 'AppController'
	});

  this.route('fixedGear', {
    path:'/fixedGear',
    controller: 'AppController',
    template: 'fixedGear',
    renderTemplates: {
      'defaultHeader': {to: 'header', data: {title: 'Bike - fixed gear'}},
    }
  });

  this.route('tilesDemo', {
    path:'/tilesDemo',
    controller: 'AppController',
    template: 'tilesDemo',
    renderTemplates: {
      'defaultHeader': {to: 'header', data: {title: 'Tiles demo'}}
    },
    data: function() {
      return { 
        content_cls: "my-page" 
      };
    }
  });

  this.route('popupDemo', {
    path:'/popupDemo',
    controller: 'AppController',
    renderTemplates: {
      'defaultHeader': {to: 'header', data: {title: 'Popup demos'}},
      'defaultFooter': {to: 'footer'}
    }
  });

  this.route('fontAwesomeDemo', {
    path: '/fontawesomedemo',
    controller: 'AppController',
    before: function(){
      if ( ReactiveAmplifyStore.get("curIconClass") === undefined ) //set default if nothing is defined
        ReactiveAmplifyStore.set("curIconClass", 'fa-search');

      console.log(ReactiveAmplifyStore.get("curIconClass"), "curIconClass");
    },
    renderTemplates: {
      'defaultHeader': {to: 'header', data: {title: 'Fontawesome icon demo'}},
      'defaultFooter': {to: 'footer'}
    }
  });

  // ----------------------------------------------------------

  /* ---------------------- post routes -------------------- */
  
  this.route('posts', {
      path: '/posts',
      template: 'newPosts',
      controller: 'PostsController',
  });

  this.route('newPosts', {
      path: '/posts/new',
      template: 'newPosts',
      controller: 'PostsController'
  });


  this.route('bestPosts', {
      path: '/posts/best',
      template: 'bestPosts',
      controller: 'PostsController'
  });

  this.route('postDetails', {
    path: '/posts/:_id',
    controller: 'PostController',
    waitOn: Subscriptions['comments'].ready()
  });

  this.route('postSubmit', {
    path: '/submit/',
    controller: 'PostsController',
    template: 'postSubmit'
  });

  this.route('postEdit', {
    path: '/posts/:_id/edit',
    controller: 'PostController',
    template: 'postEdit',
    requireLogin: true
  });
});

Router.configure({
    layout: 'layout',
    notFoundTemplate: 'notFound'
    // loadingTemplate: 'loading' // not used
});

RouteFilter = [];
RouteFilter.requireLogin = {only: ['postSubmit','postEdit']};

RouteFilter.push( function() {
  Session.set('changePage', null);
  //Session.set('hideRightPanelBtn', false);

  // Show's the jQuery Mobile loading icon
  $.mobile.loading( "show" );
  
  this.clearErrors();
});

// RouteFilter.push( [ function() {
//   //require login
//   console.log(Meteor.user());
//   console.log("requireLogin only runs on edit / submit");
//   // console.log(Router.current(), "current Router");
//   // var curRouteName = Router.current();
//   // console.log(RouteFilter.requireLogin.only);
//   // console.log(curRouteName.route.name, "curRouteName");
//   // var loginRequired = (_.without(RouteFilter.requireLogin.only, curRouteName).length > 0);
//   // console.log(loginRequired, "login require");

//   // if (Meteor.user() || !this.requireLogin )      
//   //     return;    
//   // else if (Meteor.loggingIn())      
//   //   return 'loading';    
//   // else      
//   //   this.stop();
//   //   return Errors.throw("accessDenied!"); //'accessDenied';
// }, 
// {
//   except: ['home', 'posts', 'postDetails', 'newPosts', 'bestPosts']
// }] );

// this hook will run on almost all routes
//Router.before(mustBeSignedIn, {except: ['login', 'signup', 'forgotPassword']});

AppController = RouteController.extend({
  before: RouteFilter,
  after: function(){
    //console.log(this, "after run started");
    this.setTemplateClasses();
    Session.set('changePage', "#jQueryPage");
  },
  renderTemplates: {
    'defaultFooter': {to:'footer'}
  },
  setTemplateClasses: function(options) {
    if ( typeof this.data == 'function' ) {
      var dataObj = this.data();
      Session.set('page_cls', dataObj.page_cls); 
      Session.set('content_cls', dataObj.content_cls);
    } else { // nothing defined --> clear session vars
      this.clearTemplateClasses();
    }
  },
  clearTemplateClasses: function() {
    Session.set('page_cls', ""); 
    Session.set('content_cls', "");
  },
  clearErrors: function() {    
    Errors.clearSeen();    
  }
});


PostController = AppController.extend({
  template: 'postDetails',
  renderTemplates: {
    'postDetailsHeader': {to: 'header'},
    'postDetailsFooter': {to: 'footer'}
  },
  data: function() {
    // console.log(this.params._id);
    Session.set('currentPostId', this.params._id);
    return {
      post: Posts.findOne(this.params._id),
      content_cls: "postedit-page"
    };
  }
});

PostsController = AppController.extend({
  after: function(){
    //console.log(this, "after run started");
    this.setTemplateClasses();
    Session.set('changePage', "#jQueryPage");
    //AppController.after(); //<<<<<<<<<<<not workin
    Session.set('currentPostId',null); //clear currenPostId
  },
  renderTemplates: {
    'defaultHeader': {to: 'header', data: {title: 'Posts'}},
    'defaultFooter': {to: 'footer'}
  },
  waitOn: Subscriptions[this.template],
  data: function() {
    return { 
      handle: Subscriptions[this.route.name],
      content_cls: "posts-page"
    };
  }
});

//route filters not working with iron-router --> needs to be added to before AppController
/*Meteor.Router.filters({  
  'requireLogin': function(page) {    
    if (Meteor.user())      
      return page;    
    else if (Meteor.loggingIn())      
      return 'loading';    
    else      
      return 'accessDenied';  
  },  
  'clearErrors': function(page) {    
    //clearErrors();
    Errors.clear();    
    return page;  
  }
});

Meteor.Router.filter('requireLogin', {only: 'postSubmit'});
Meteor.Router.filter('clearErrors');
*/