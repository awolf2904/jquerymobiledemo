Handlebars.registerHelper('pluralize', function(n, thing) {
  return jQmDemo.pluralize(n, thing);
});

Handlebars.registerHelper('encodeURL', function(url) {
	// console.log(encodeURIComponent(url).replace(/[!'()]/g, escape).replace(/\*/g, "%2A"));
	return encodeURIComponent(url).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
});

Handlebars.registerHelper('readableDate', function(dateRaw){
    return jQmDemo.convertDate(dateRaw);
});

/*Handlebars.registerHelper('incIndex', function(){
    this.index = eachIndex++;
});*/

/*Handlebars._default_helpers.each = function(data, options){
    var parentData = this;
    console.log("this hbs each override", data);
    if (data && data.length > 0)
      return _.map(data, function(x, i) {
      	if (data) { data.index = i; }
        // infer a branch key from the data
        var branch = ((x && x._id && idStringify(x._id)) ||
                      (typeof x === 'string' ? x : null) ||
                      Spark.UNIQUE_LABEL);
        return Spark.labelBranch(branch, function() {
          return options.fn(x);
        });
      }).join('');
    else
      return Spark.labelBranch(
        'else',
        function () {
          return options.inverse(parentData);
        });
};*/

/*// handlebars each helper
Handlebars.registerHelper('each_test', function(context, options) {
  console.log("each_test started", context);
  var fn = options.fn, inverse = options.inverse;
  var ret = "", data;

  if (options.data) {
    data = Handlebars.createFrame(options.data);
  }
  console.log(data);
  if(context && context.length > 0) {
    for(var i=0, j=context.length; i<j; i++) {
      if (data) { data.index = i; }
      ret = ret + fn(context[i], { data: data });
    }
    console.log(context);
  } else {
    ret = inverse(this);
    console.log(this)
  }
  console.log(ret);
  return ret;
});*/