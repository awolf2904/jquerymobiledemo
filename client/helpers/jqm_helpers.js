/*
* jQueryMobiledemo - Show case for jQuery Mobile and Meteor
* http://jquerymobiledemo.meteor.com
*
* Copyright 2013 A. Wolf / AWolf2904@gmail.com
* Released under the GNU AGPL license.
* <http://www.gnu.org/licenses/>
*
*/

jQmDemo = {};
jQmDemo.saveText = function(e, storeName, $el) {
  // console.log("test");
  var storedText = ReactiveAmplifyStore.get(storeName);
  var curChar = String.fromCharCode(e.keyCode);
  if (!storedText){
      //empty store
      ReactiveAmplifyStore.set(storeName,curChar);
  }
  else
  {
      storedText = $el.val();
      ReactiveAmplifyStore.set(storeName, storedText);
  }
  // console.log(storedText); //String.fromCharCode(e.keyCode));//$('#textinput-2').val());
};

jQmDemo.convertDate = function(raw) {
  //console.log(new Date(raw).toString());
  return new Date(raw).toString();
};


// e.g. speed = 'slow'
jQmDemo.scrollto = function(element, speed){
  if ( speed === null ) 
    speed = 'slow';

  $('html, body').animate({ scrollTop: ($(element).offset().top)}, speed);
};

jQmDemo.pluralize = function(n, thing) {
  // fairly stupid pluralizer
  if (n === 1) {
    return '1 ' + thing;
  } else {
    return n + ' ' + thing + 's';
  }
}

jQmDemo.styleSheetContains = function (f) {
    var hasstyle = false;
    var fullstylesheets = document.styleSheets;
    for (var sx = 0; sx < fullstylesheets.length; sx++) {
        var sheetclasses = fullstylesheets[sx].rules || document.styleSheets[sx].cssRules;
        for (var cx = 0; cx < sheetclasses.length; cx++) {
            if (sheetclasses[cx].selectorText == f) {
                hasstyle = true; break;
                //return classes[x].style;              
            }
        }
    }
    return hasstyle;
};