/*
* jQueryMobiledemo - Show case for jQuery Mobile and Meteor
* http://jquerymobiledemo.meteor.com
*
* Copyright 2013 A. Wolf / AWolf2904@gmail.com
* Released under the GNU AGPL license.
* <http://www.gnu.org/licenses/>
*
*/

Session.setDefault('changePage', null);
Session.setDefault('data_loaded', false);
Session.setDefault('currentPostId', null);
//Session.setDefault('hideRightPanelBtn', false);


updatePage = function(){
  var changeTarget = Session.get('changePage'); //selector for jquery mobile change page
  
  if ( changeTarget &&                                // target not null
       $.mobile.activePage) {                         // jquery mobile initialized
    $.mobile.changePage( $(changeTarget) , {
                                            reverse: false,
                                            changeHash: false,
                                            allowSamePageTransition: true,
                                            reloadPage: true
    });
  }
};

// make jQmobile work with meteor reactivity --> refresh jQm after every render 

defaultRendered = function() {

  //updatePage();
  //console.log(this.findAll("[data-rel='popup']"), "defaultRendered findAll");

  var newPage = Session.get('changePage');
  var triggerPage = function(triggerEl) {
    if ( triggerEl ) {
      $(triggerEl).trigger('create'); // Always refresh widgets after rendering  
    }
  };
  var refreshLists = function(){
    //console.log($('.ui-listview:visible'), "listviews");
    if ( $(".ui-listview") ) {
      $(".ui-listview").listview("refresh");
    }
  };

  triggerPage(newPage);

  if (!this.isRendered){
    // run my code only once --> check reactive re-rendering separately
    // --> not used yet
    //this.rendered = true;
    console.log(newPage, "newpage to show - first render");
    //triggerPage(newPage);

    this.isRendered = true;
  } else {   // first render done --> check if re-render required
    console.log(newPage, "newpage to show, re-render");// + clsName);
    // if ( $(".ui-listview-filter:visible") ) // a bit tricky, the problem with filter is that it is generated outside of the ul-tag --> not properly re-rendered
    //   $(".ui-listview-filter:visible").remove(); //remove list search form --> avoids duplicates
    
    //triggerPage(newPage);
    refreshLists();
  }
  // init popups (not working yet)
  // var popups = this.findAll("[data-rel='popup']");
  // console.log(popups);
  
};

Deps.autorun(function() {
  updatePage();
});

// attach defaultRendered to main layout template is working (no need to add it to sub-templates)

Template['layout'].rendered = _.wrap(Template['layout'].rendered, defaultRendered); //adds defaultRendered to layout template.rendered
// wrapping not working as intended --> it's not possible to use another layout.rendered function

Meteor.startup(function() {
  //commentHandle = Meteor.subscribe('comments', Session.get('currentPostId'));

  // console.log("startup !!");
});