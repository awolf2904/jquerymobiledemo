Template.notifications.helpers({
	curNotifications: function(){
		return Notifications.find({userId: Meteor.userId(), read: false});
	},
	notificationCount: function(){
		this.notifyCount = Notifications.find({userId: Meteor.userId(), read:false}).count();
		return this.notifyCount;
	},
	notificationTooltip: function(){
		//var notifyCount = 0; //Template.notifications.notificationCount();
		// console.log(this, "hdbs obj");
		// console.log(this.pluralize(this.notifyCount, "notification"), "plural hdbs");
		if ( this.notifyCount )
			return "You have " + jQmDemo.pluralize(this.notifyCount, "notification") + "!";
		else
			return "No notifications, at the moment.";
	}
});

Template.notification.helpers({
	commentedPostName: function(){
		return Posts.findOne(this.postId).title;
	}
});

Template.notification.events({
	'click a': function() {
		//Notifications.update(this._id, {$set: {read:true}});
	  Notifications.remove(this._id);
	}
});