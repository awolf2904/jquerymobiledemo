Template.bestPosts.helpers({
	options: function(){
		return {
			sort: {votes: -1, submitted: -1},
			handle: Subscriptions['bestPosts']
		};
	}
});

Template.newPosts.helpers({
	options: function(){
		return {
			sort: {submitted: -1},
			handle: Subscriptions['newPosts']
		};
	}
});

Template.postsList.helpers({
	posts: function(){
		var options = {sort: this.sort, limit: this.handle.limit()};
		return Posts.find({}, options);
	}, 
	postsReady: function(){ // --> maybe also works only with waitOn in controller
		var isReady = this.handle.ready();
		if ( !isReady ) {
			// Show's the jQuery Mobile loading icon
  		$.mobile.loading( "show" );
  	} else {
  		$.mobile.loading( "hide" );
  	}
		return isReady;
	},
	allPostsLoaded: function(){
		return	! this.handle.loading() &&
						Posts.find().count() < this.handle.loaded();
	}
});

Template.postsList.events({
  'click .load-more': function(e) {
    e.preventDefault();
    this.handle.loadNextPage();
  }
});