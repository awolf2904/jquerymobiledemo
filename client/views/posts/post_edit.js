Template.postEdit.helpers({
	post: function() {
		//var currentPost = Posts.findOne(Session.get('currentPostId'));
		//console.log(Posts.findOne(Session.get('currentPostId')).message);
		//console.log(Posts.findOne(Session.get('currentPostId')));
		//console.log(Session.set('currentPostId'));
		return Posts.findOne(Session.get('currentPostId'));
	}
});

Template.postEdit.deleteItem = function(context) {
	if ( confirm("Delete this post?") ) {
			//var currentPostId = Session.get('currentPostId');
			Posts.remove(context._id);//currentPostId);
			//Meteor.Router.to('home');
			Router.go('posts');
		}
};

Template.postEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var currentPostId = Session.get('currentPostId');
		// console.log(currentPostId);
		var postProperties = {
			url: $(e.target).find('[name=url]').val(),
			title: $(e.target).find('[name=title]').val(),
			message: $(e.target).find('[name=message]').val(),
		};

		Posts.update(currentPostId, {$set: postProperties}, function(error){
			if ( error ) {
				// display the error to the user
				alert (error.reason);
			} else {
				//Meteor.Router.to('postPage', currentPostId);
				Router.go('postDetails', {_id: currentPostId});
			}
		});
	},

	'click .delete': function(e){
		e.preventDefault();
		//console.log(this);
		Template.postEdit.deleteItem(this);
/*		if ( confirm("Delete this post?") ) {
			var currentPostId = Session.get('currentPostId');
			Posts.remove(currentPostId);
			Meteor.Router.to('postsList');
		}*/
	},

	'click .cancel': function(e){
			e.preventDefault();
			//Meteor.Router.to('/posts/' + this._id);
			Router.go('/posts/' + this._id);
	}
});