//post_item.js


Template.postItem.helpers({
	ownPost: function() {
		//console.log(this);
		//console.log(this.userID);
		//console.log(Meteor.userId());
		return this.userId == Meteor.userId();
	},
	
	domain: function(){
		var a = document.createElement('a');
		a.href = this.url;
		return a.hostname;
	},

	upvotedClass: function() {
    var userId = Meteor.userId();
    if (userId && !_.include(this.upvoters, userId)) {
      return 'btn-primary upvotable';
    } else {
      return 'ui-disabled';
    }
  },

  singlePost: function() {
		return Session.get('currentPostId');
  }

	/*,
	commentsCount: function(){
		return Comments.find({postId: this._id}).count();
	}*/

	/*,

	author: function(){
		//var currentPostId = Session.get('currentPostId');
		//var currentUserId = Posts.findOne(currentPostId);
		//console.log(currentUserId);
		return "Test"; //Meteor.users.findOne(currentUserId).userName;
	}*/
});

Template.postItem.rendered = function() {

	// add ellipsis jQueryplugin to title of item
	// console.log(Session.get('currentPostId'));
	if ( Session.get('currentPostId') === null ) { //only if not single post view
		$(".ellipsis").ellipsis({
	    row: 5
		});
	}

	//animate post from previous to new position
	var instance = this;
	var rank = instance.data._rank;
	var $this = $(this.firstNode);
	var postHeight = 80;
	var newPosition = rank * postHeight;
	//if element has a current Position (i.e. it's not the first ever render)
	if ( typeof(instance.currentPosition) !== 'undefined') {
		var previousPosition = instance.currentPosition;
		// calculate difference between old position and new position and send element there
		var delta = previousPosition - newPosition;
		$this.css("top", delta + "px");
	} else {
		// it's the first ever render, so hide element
		$this.addClass("invisible");
	}
	// let it draw in the old postition, then..
	Meteor.defer(function(){
		instance.currentPosition = newPosition;
		// bring element back to its new original position
		$this.css("top", "0px").removeClass("invisible");
	});
};

Template.postItem.events({
	'click .upvotable': function(e) {
		e.preventDefault();
		console.log(this._id);
		Meteor.call('upvote', this._id);
		Deps.flush();
		$(".post-list").listview('refresh');
	},

	'click #btnRemovePost': function(e){
		e.preventDefault();
		//console.log(this);
		Template.postEdit.deleteItem(this);
	},

	'click .infoTeamMembers' : function(e){
		$('.allPops').not(e.currentTarget).popover('hide');
	}

/*
	'mouseenter a[rel=tooltip]': function(){
		$("a[rel=tooltip]").tooltip({ placement: 'bottom'});
	}*/
});

