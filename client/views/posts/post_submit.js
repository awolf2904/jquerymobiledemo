Template.postSubmit.events({
	'submit form': function(e){
		e.preventDefault();
		//console.log(Meteor.user());

		var post = {
			url: $(e.target).find('[name=url]').val(),
			title: $(e.target).find('[name=title]').val(),
			message: $(e.target).find('[name=message]').val(),
			// visibility: $(e.target).find('[name=privacyInput]').val()
		};

		// console.log($(e.target).find('[name=privacyInput]').val(), "etarget privacyInput");
		// console.log(post.visibility);

		Meteor.call('post', post, function(error, id){
			if ( error ) {
				//return alert(error.reason);
				// display the error to the user
				Errors.throw(error.reason);
				// if the error is that the post already exists, take us there
				if (error.error === 302) 
					console.log(error.details);
					Router.go('postDetails', error.details);
				} else {
				//Meteor.Router.to('postPage', id);
				Router.go('postDetails', {_id: id});
			}
		});
		//Meteor.Router.to('postsList'); 
	}

});

/*Template.postSubmit.rendered = function() {
	$(".ui-listview").listview('refresh');
};*/