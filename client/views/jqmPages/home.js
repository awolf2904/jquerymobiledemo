/*
* jQueryMobiledemo - Show case for jQuery Mobile and Meteor
* http://jquerymobiledemo.meteor.com
*
* Copyright 2013 A. Wolf / AWolf2904@gmail.com
* Released under the GNU AGPL license.
* <http://www.gnu.org/licenses/>
*
*/

formDefaults = {
  "slider"      : "0",
  "checkbox"    : [false, false, false, false],
  "slider2"     : "off",
  "textarea-2"  : "Textarea text",
  "radiochoice1": "radio-choice-1",
  "textinput-2" : "Textinput text"
};

Template.home.events({
  'click #btnReset': function(e){
    e.preventDefault();
    _.each( amplify.store(), function(obj, key) {
      ReactiveAmplifyStore.set(key, formDefaults[key]); // reactivity required here --> re-renders all elements
      //amplify.store(key, formDefaults[key]);          // also works because reacitveAmplifyStore is called too.
    });
  }
});

Template.home.rendered = function() {

  // ------------------------- init controls -----------------------

  //radio init
  $("#radio-" + ReactiveAmplifyStore.get("radiochoice1")).prop("checked",true).checkboxradio("refresh");

  //slider
  $('#slider-fill-mini').val(ReactiveAmplifyStore.get("slider")).slider("refresh");
  
  //flip switch slider 2
  $('#slider2').val(ReactiveAmplifyStore.get("slider2")).slider('refresh');
  
  //checkbox init
  var curChkSelection = []; // [0] = checkbox-1a , [1] = 2a ...

  curChkSelection = ReactiveAmplifyStore.get('checkbox');

  if ( curChkSelection ) {
    for ( var index in curChkSelection ) {
      if ( curChkSelection[index] ) {
        $("#checkbox-" + (Number(index) + 1) + "a").prop("checked", true);
        //console.log($("#checkbox-" + (Number(index) + 1) + "a").prop("checked"), "input [name=checkbox-" + (Number(index) + 1) + "a] checkState");
      }
      else
      {
        //console.log($("#checkbox-" + (Number(index) + 1) + "a")); // input [name=checkbox-" + (Number(index) + 1) + "a]");
        var $el = $("#checkbox-" + (Number(index) + 1) + "a");
        //console.log($el, "$element");
        if ( $el.is(":checked") ) { //remove checked only if it is really checked
            //$el.setAttribute("checked", ""); // For IE
            //$el.removeAttribute("checked"); // For other browsers
            $el.prop("checked", false);
            //$el.checked = false;
        }
      }
    }
  }
  else
  { //checkbox undefined --> create from page markup
    var newChkSelection = []; // don't use curChkSelection here --> deps.flush issue
    var $checkBoxes = $('input[type=checkbox]');
    
    $checkBoxes.each(function() {
        newChkSelection.push(this.checked);
    });

    ReactiveAmplifyStore.set('checkbox', newChkSelection);
  }

  // text fields init
  var storedText = ReactiveAmplifyStore.get('textinput-2');
  $('#textinput-2').val(storedText);

  storedText = ReactiveAmplifyStore.get('textarea-2');
  $('#textarea-2').val(storedText);

  // --------------
  $('input[name=radio-choice-1]').on('change', function(e) {
    var newValue = $(e.currentTarget).val();
    ReactiveAmplifyStore.set("radiochoice1", newValue);
  });

  $('input[type=checkbox]').on('change', function(e) {
    var regExpr = /\d+/;
    curChkSelection = ReactiveAmplifyStore.get('checkbox');

    var newValue = $(this);
    var i = e.currentTarget.id.match(regExpr);

    if ( isNumber(i) ) {
      curChkSelection[i-1] = newValue.is(':checked');
      ReactiveAmplifyStore.set('checkbox', curChkSelection);
    }
  });

  $('#slider-fill-mini').on('change', function(e) {       // change or slidestart or slidestop
      var newValue = $(this);
      ReactiveAmplifyStore.set("slider",newValue.val());
      $("#jQueryPage").data("hasSliderChanged", true);    // required to avoid panel swiping
  });

  $('#slider2').on('change', function(e) {                // change or slidestart or slidestop
    var newValue = $(this);
    ReactiveAmplifyStore.set("slider2", newValue.val());  // flip switch
    $("#jQueryPage").data("hasSliderChanged", true);      // required to avoid panel swiping
  }); 

  $('#textinput-2').on('keyup', function(e){
    jQmDemo.saveText(e, "textinput-2", $(this));
  });
  
  $('#textarea-2').on('keyup', function(e){
    jQmDemo.saveText(e, "textarea-2", $(this));
  });
};