Template.commentSubmit.events({
	'submit form': function(e, template){
		e.preventDefault();
		e.stopPropagation();
		//console.log(Meteor.user());

		var $body = $(e.target).find('[name=body]'); 
		
		var comment = {
			body: $body.val(),
			postId: template.data._id	
		}

//		insert directly date stamping etc. not possible
//		post._id = Posts.insert(post);

		Meteor.call('comment', comment, function(error, commentId){
			if ( error ) {
				//return alert(error.reason);
 				// display the error to the user
 				throwError(error.reason);
 				// if the error is that the post already exists, take us there
 				//if (error.error === 302) Meteor.Router.to('postPage', error.details)
			//Meteor.Router.to('postPage', post);
			} else {
				$body.val('');
				//$('body').scrollTo($body); //scroll to input 
				jQmDemo.scrollto('.addComment', 0);
				//Meteor.Router.to('postPage', id);
			}
		});
		//Meteor.Router.to('postsList'); 
	}/*,

	'click .scrollTest': function(e) {
		e.preventDefault();
		jQmDemo.scrollto('.ui-li-heading');
	}*/

});