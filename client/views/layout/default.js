Template.layout.helpers({
  shareText: function() {
    return "Great jQuery mobile demo with Meteor!";
  },
  shareTarget: function() {
    var currHref = window.location.href;
    return currHref;
  },
  page_cls: function() {
  	return Session.get('page_cls');
  },
  content_cls: function() {
  	return Session.get('content_cls');
  },
  currentUser: function(){ //normally currentUser should be available with-out this, but it wasn't
    var user = Meteor.user();
    //console.log(Meteor.user());
    console.log(user);
    //return "" || user;
  }
});

// Template['layout'].rendered = function() {
//   console.log("test");
//   $('#btnLogin').button({icons: {primary: 'icon-user'}});
// };