Template.topPostNav.helpers({
	activeRouteClass: function() { /*route names are args*/
		var args = Array.prototype.slice.call(arguments,0);
		args.pop();
		//console.log(args); //home, newPosts
		var active = _.any(args, function(name) {
			/*console.log(Router.path(name), name);
			console.log(location.pathname, "pathname loc");*/
			return location.pathname === Router.path(name);//location.pathname === Meteor.Router[name+ 'Path']();
		}); //underscore helper if any of the args arre is true then active = true 
	
		return active ? 'active': ''; // boolean and JS pattern --> false && mystring returns false but true && myString returns myString
	}

});