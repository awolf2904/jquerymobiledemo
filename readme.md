# About the repo
I'm pretty new to git so it's possible that the repo is not as good structured as it should be especially in the devel branch. It's my first git repository. So I'm still learning everything and I'm trying to do it as good as I can.

# Resources that helped making this demo
I'm pretty new to jQuery, jQueryMobile, Meteor and JavaScript.
The book [discoverMeteor](http://book.discovermeteor.com/ "discoverMeteor") is very good to learn Meteor. The "show posts" part in this demo (called Microscope in the book) is the demo app that you'll create with the book. Check out the book it is worth every cent.

The EventedMind webpage is also a great site with screencast for learning meteor. The examples in the casts are very well explained and always showing the detailed steps to implement meteor features (e.g. one shows how Iron-router works).

For learning JS check out [codeacedemy.com](http://www.codecademy.com/). The courses are great and it makes fun to do the courses. Because there's always something to code and so you can learn by doing.

# Reactivity
Just the posts section has reactivitiy with Meteor collections. (See left panel navigation in "show posts")

On the home page I'm showing some elements that are saved with amplify locally. Reactivity is also used for the amplify store. (See "Reset form elements" button.) It uses a reacitve data source. Extending the Session class would also work.

# License
The repo is released under the GNU AGPL license.

If someone is improving the code so that it's easier to use with Meteor. Just send me a message with a link to the fork. So I can check it and I could also dual license that repo under MIT.

It would be great to have it as a meteorite package (MIT would be required), but I think it's not ready for a package. It could be ready but I haven't checked how to do a smart package yet.

# Todo
- Need to change the position of the votes of the madewith votes/comments
- Jqm popups still not working

# Contribute
If you'd like to contribute to the demo please check the issue section and check if there's an open issue that you could work on.
Or fork the repo and implement some jQuery mobile features and send me a pull request. There are probably many features of jQm that aren't working properly yet.