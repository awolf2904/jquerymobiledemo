Package.describe({
  summary: "Simple jQuery mobile package for Meteor"
});

Package.on_use(function (api, where) {
  api.use(['templating', 'jquery', 'handlebars'], 'client');
  //api.use('jquery', 'client');
  //api.use('deps', 'client');
  //api.use('startup', 'client');
  
  api.add_files([
    //add the main file that contains the configuration for meteor
    'lib/main.js',

    //add jquery mobile css and js files
    'lib/jquery.mobile-1.3.2.min.css',
    
    'lib/jquery.mobile-1.3.2.js', //.min later
    

    // add image files
    'lib/images/ajax-loader.gif',
    'lib/images/icons-18-black.png',
    'lib/images/icons-18-white.png',
    'lib/images/icons-36-black.png',
    'lib/images/icons-36-white.png',
    ], 'client');
  //if (api.export) 
  //  api.export('Raty');
});

/*
Package.on_test(function(api) {
  api.use('errors', 'client');
  api.use(['tinytest', 'test-helpers'], 'client');

  api.add_files('errors_tests.js', 'client');
});
*/
// call testing witth meteor test-packages raty