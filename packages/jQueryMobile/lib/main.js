$(document).on("mobileinit", function () {
		$.mobile.ajaxEnabled					= false;
		$.mobile.hashListeningEnabled	= false;
		$.mobile.pushStateEnabled			= false;
		$.mobile.linkBindingEnabled		= false; // delegating all the events to chaplin
		$.mobile.changePage.defaults.changeHash = false;

		$.mobile.defaultDialogTransition	= "none";
		$.mobile.defaultPageTransition		= "slide";// none "slide"; //"slidedown";
		$.mobile.page.prototype.options.degradeInputs.date = true;
		$.mobile.page.prototype.options.domCache	= false;

		$.mobile.ignoreContentEnabled	=	true;
    //$.mobile.autoInitializePage = false; //manual call to init page with $.mobile.initializePage is required

    $("div[data-role='page']").on("pagehide", function (event, ui) {
			console.log("pagehide triggered!");
			$(event.currentTarget).remove();
		});

		/*$(document).on('pagebeforeshow', '#jQueryPage', function(){

			//add elements here

			$('#jQueryPage').trigger('pagecreate');
		
		});*/
		//console.log('mobile init done!');
		$( document ).on( "swipeleft swiperight", "#jQueryPage", function( e ) {
        // We check if there is no open panel on the page because otherwise
        // a swipe to close the left panel would also open the right panel (and v.v.).
        // We do this by checking the data that the framework stores on the page element (panel: open).
        
        //console.log($("#jQueryPage").data("hasSliderChanged"),"swipe");
        //was Change event fired first? If so, do nothing except resetting the flag.
        if($("#jQueryPage").data("hasSliderChanged")){
          //reset
          // console.log($("#jQueryPage").data("hasSliderChanged"),"swipe");
          $("#jQueryPage").data("hasSliderChanged", false);
        } else {
          if ( $.mobile.activePage.jqmData( "panel" ) !== "open" ) {
            if ( e.type === "swipeleft"  ) {
              $( "#right-panel" ).panel( "open" );
            } else if ( e.type === "swiperight" ) {
              $( "#left-panel" ).panel( "open" );
            }
          }
        }
    });

});


// handle hot code push and reset change page session

/*if ( Meteor._reload && Meteor.isClient ) {
	Meteor._reload.onMigrate('jQm', function () {
		//return [true, {sessionId: Meteor.Presence.sessionId}];
		Session.set('changePage', null);

		console.log("hot code push reloading...");
		return [true, null];
	});
}*/